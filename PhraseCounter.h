//
// Created by Nikita on 16.10.2019.
//

#ifndef PHRASES_PHRASE_COUNTER_H
#define PHRASES_PHRASE_COUNTER_H

#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <fstream>

class PhraseCounter
{
    std::map <std::string, int> phrase_frequencies;
    int phrase_length;
    int min_freq;
    std::string file_name;

public:
    void parse_args(int argc, char const ** argv);
    void count_frequences (std::istream &input);
    void print_phrases (std::ostream &output);
    int get_phrase_length ();
    int get_min_freq ();
    std::string get_file_name ();
    void set_phrase_length (int length);
    void set_min_freq (int repeats);
    void set_file_name (std::string fname);

    PhraseCounter(int argc, char const **argv, std::istream &default_stream)
    {
        min_freq = 2;
        phrase_length = 2;
        file_name = "";
        parse_args(argc, argv);
        if (get_file_name().size() > 0)
        {
            std::ifstream in_file;
            in_file.open(get_file_name());
            count_frequences(in_file);
        }
        else
            count_frequences(default_stream);

    }
};

#endif //PHRASES_PHRASE_COUNTER_H
