#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "PhraseCounter.h"
#include <sstream>

TEST_CASE("PhraseCounter.parse_args_default", "[PhraseCounter]")
{
    std::stringstream stream;
    PhraseCounter test(0, NULL, stream);
    REQUIRE(test.get_file_name().compare("") == 0);
    REQUIRE(test.get_phrase_length() == 2);
    REQUIRE(test.get_min_freq() == 2);
}

TEST_CASE("PhraseCounter.parse_args:no_file_1", "[PhraseCounter]")
{
    int argc = 5;
    char const *argv[5] = {(char const *)"smth", (char const *)"-n", (char const *)"3", "-m", "5"};
    std::stringstream stream;
    PhraseCounter test(argc, argv, stream);
    REQUIRE(test.get_file_name().compare("") == 0);
    REQUIRE(test.get_phrase_length() == 3);
    REQUIRE(test.get_min_freq() == 5);
}

TEST_CASE("PhraseCounter.parse_args:no_file_2", "PhraseCounter")
{
    int argc = 6;
    char const *argv[6] = {"smth", "-m", "5", "-", "-n", "3"};
    std::stringstream stream;
    PhraseCounter test(argc, argv, stream);
    REQUIRE(test.get_file_name().compare("") == 0);
    REQUIRE(test.get_phrase_length() == 3);
    REQUIRE(test.get_min_freq() == 5);
}

TEST_CASE("PhraseCounter.parse_args:with_file", "[PhraseCounter]")
{
    int argc = 6;
    char const *argv[6] = {"smth", "filename", "-n", "3", "-m", "5"};
    std::stringstream stream;
    PhraseCounter test(argc, argv,stream);
    REQUIRE(test.get_file_name().compare("filename") == 0);
    REQUIRE(test.get_phrase_length() == 3);
    REQUIRE(test.get_min_freq() == 5);
}

TEST_CASE("PhraseCounter.set&get", "[PhraseCounter]")
{
    std::stringstream stream;
    PhraseCounter test(0, NULL, stream);
    test.set_phrase_length(3);
    test.set_min_freq(5);
    test.set_file_name("filename");
    REQUIRE(test.get_file_name().compare("filename") == 0);
    REQUIRE(test.get_phrase_length() == 3);
    REQUIRE(test.get_min_freq() == 5);
}

TEST_CASE("PhraseCounter.count_frequences&output_0", "[PhraseCounter]")
{
    std::stringstream stream_in;
    stream_in << "Never gonna give you up\n"
              "Never gonna let you down\n"
              "Never gonna run around and desert you\n"
              "Never gonna make you cry\n"
              "Never gonna say goodbye\n"
              "Never gonna tell a lie and hurt you\n";
    stream_in << "Hey now, you're an all-star, get your game on, go play\n"
                 "Hey now, you're a rock star, get the show on, get paid\n"
                 "And all that glitters is gold\n"
                 "Only shooting stars break the mold\n";
    PhraseCounter test(0, NULL, stream_in);
    std::stringstream stream_out;
    test.print_phrases(stream_out);
    REQUIRE(stream_out.str().compare("Never gonna (6)\n"
                                     "Hey now, (2)\n"
                                     "now, you're (2)\n") == 0);
}


TEST_CASE("PhraseCounter.count_frequences&output", "[PhraseCounter]")
{
    std::stringstream stream_in;
    PhraseCounter test(0, NULL, stream_in);
    test.set_phrase_length(5);
    test.set_min_freq(1);
    test.set_file_name("");
    stream_in << "Pam pam pam";
    test.count_frequences(stream_in);
    std::stringstream stream_out;
    test.print_phrases(stream_out);
    REQUIRE(stream_out.str().compare("") == 0);
}