//
// Created by Nikita on 16.10.2019.
//

#include <algorithm>
#include "PhraseCounter.h"

void PhraseCounter :: count_frequences(std::istream &input)
{
    std::string cur_phrase;
    std::string word;
    int i = 0;

    while (i < phrase_length - 1 && input >> word)
    {
        cur_phrase += word + ' ';
        i++;
    }

    while (input >> word)
    {
        cur_phrase += word + ' ';
        phrase_frequencies[cur_phrase]++;
        cur_phrase = cur_phrase.substr(cur_phrase.find(' ') + 1,  std::string::npos);
    }
}

void PhraseCounter :: print_phrases (std::ostream &output)
{
    std::vector <std::pair <std::string, int>> sorted;

    for (auto &phrase : phrase_frequencies)
    {
        if (phrase.second >= min_freq)
            sorted.push_back(phrase);
    }

    std::sort(sorted.begin(), sorted.end(), [](const std::pair<std::string, int> &x,
                                           const std::pair<std::string, int> &y)
    {
        return x.second > y.second;
    });

    for (auto &phrase : sorted)
    {
        output << phrase.first << "(" << phrase.second << ")\n";
    }
}

void PhraseCounter :: parse_args(int argc, char const** argv)
{
    for (int i = 1; i < argc; i++)
    {
        if (argv[i][0] == '-')
        {
            switch (argv[i][1])
            {
                case 'n':
                    phrase_length = atoi(argv[i + 1]);
                    i++;
                    break;
                case 'm':
                    min_freq = atoi(argv[i + 1]);
                    i++;
                    break;
                default:
                    break;
            }
        }
        else
            file_name = argv[i];
    }
}

int PhraseCounter :: get_phrase_length ()
{
    return phrase_length;
}

int PhraseCounter :: get_min_freq ()
{
    return min_freq;
}

std::string PhraseCounter :: get_file_name ()
{
    return file_name;
}

void PhraseCounter :: set_phrase_length (int length)
{
    phrase_length = length;
}

void PhraseCounter :: set_min_freq (int repeats)
{
    min_freq = repeats;
}

void PhraseCounter :: set_file_name (std::string fname)
{
    file_name = fname;
}
