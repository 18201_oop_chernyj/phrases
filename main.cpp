#include "PhraseCounter.h"
#include <fstream>

int main(int argc, char const **argv)
{
    PhraseCounter phrase_counter(argc, argv, std::cin);
    phrase_counter.print_phrases(std::cout);
    return 0;
}